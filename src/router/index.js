import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Second from '../views/Second.vue'
import Event from '../components/Event'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home
	},
	{
		path: '/cards',
		name: 'Second',
		component: Second
	},
	{
		path: '/event/:id',
		component: Event
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		events: [],
		event: null
	},
	mutations: {
		SET_EVENTS_TO_STATE: (state, events) => {
			state.events = events
		},
		SET_EVENT_TO_STATE: (state, event) => {
			state.event = event
		}
	},
	actions: {
		GET_EVENTS_FROM_API({ commit }) {
			return axios('http://localhost:8000/events', {
				method: 'GET'
			})
				.then(events => {
					commit('SET_EVENTS_TO_STATE', events.data)
					return events
				})
				.catch(error => {
					console.log(error)
					return error
				})
		},
		GET_EVENT_FROM_API({ commit }, eventId) {
			return axios('http://localhost:8000/events/' + eventId, {
				method: 'GET'
			})
				.then(event => {
					commit('SET_EVENT_TO_STATE', event.data)
					return event
				})
				.catch(error => {
					console.log(error)
					return error
				})
		}
	},
	getters: {
		EVENTS(state) {
			return state.events
		},
		EVENT(state) {
			return state.event
		}
	}
})

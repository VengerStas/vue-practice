module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/essential", 
    "plugin:prettier/recommended",
    "@vue/prettier"
    ],
  parserOptions: {
    parser: "babel-eslint"
  },
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error",
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off"
  }
};
